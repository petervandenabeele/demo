* in the gcloud-demo-atd project on Google Cloud, add the bucket

  https://console.cloud.google.com/storage/create-bucket?project=gcloud-demo-atd

* bucket: gcloud-demo.allthingsdata.io
* Regional => europe-west1  (that is Belgium)

Bucket is visible on

  https://console.cloud.google.com/storage/browser/gcloud-demo.allthingsdata.io?project=gcloud-demo-atd

Now, based on https://cloud.google.com/storage/docs/hosting-static-website

Add the CNAME for gcloud-demo.allthingsdata.io on Gandi :

gcloud-demo 600 IN CNAME c.storage.googleapis.com.

This is not yet visible with `dig` probably waiting for the
original 10800 (3 hour) TTL ?

Make a basic static site with index.html and 404.html
(see ../static/ in this repository)

While waiting for the dsn TTL expiration, hack the local /etc/hosts file as such:

216.58.206.16     gcloud-demo.allthingsdata.io

$ ping gcloud-demo.allthingsdata.io # this is temporary !
PING gcloud-demo.allthingsdata.io (216.58.206.16): 56 data bytes
64 bytes from 216.58.206.16: icmp_seq=0 ttl=48 time=64.279 ms
...

At this moment (the next morning), the TTL has long passed
and the public DNS system serves the CNAME as expected.

Removed the /etc/hosts entry and all still works.

Now we get the expected result from Google:

A 404 with
"Anonymous users does not have storage.objects.list access to gcloud-demo.allthingsdata.io."

Push the static content manually to the bucket for now
(a CD on master branch is next).

Go to the browsing of the bucket on

https://console.cloud.google.com/storage/browser/gcloud-demo.allthingsdata.io?project=gcloud-demo-atd

And UPLOAD the 3 files in the static directory.

For the 3 files do this:
* Manually click them on "Public link"

Go to the Bucket and click on the 3 dots on the right.
Click the "Edit webiste configuration"

Set main page to index.html and error page to 404.html

Now the static page displays on the http://gcloud-demo.allthingsdata.io URL
