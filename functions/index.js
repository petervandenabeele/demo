/**
 * HTTP Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
*/

'use strict';

// partially from
// https://github.com/GoogleCloudPlatform/nodejs-docs-samples/blob/master/functions/pubsub/index.js

const PubSub = require('@google-cloud/pubsub');
const pubsub = PubSub();

const topicName = "gcloud-demo-atd-tracker";
const fullTopicName = "projects/gcloud-demo-atd/topics/" + topicName;

const Buffer = require('safe-buffer').Buffer;

var setJSONheaders = function (res) {
  res.header('Content-Type','application/json');
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
}

var response = function (req) {
  return(`Hello ${req.body.name || 'World'} from tracker v11 !\n`);
}

var postPubSub = function (name, res) {
  var topic = pubsub.topic(fullTopicName);
  var publisher = topic.publisher();
  var message = {
    key: "name",
    val: name,
    timestamp: (new Date()).toISOString()
  };
  // Publishes a message
  const dataBuffer = Buffer.from(JSON.stringify(message));
  return publisher.publish(dataBuffer)
    .then(() => res.status(200).send(JSON.stringify({status: "ok", message: ("Message published with name " + name)})))
    .catch((err) => {
      console.error(err);
      res.status(500).send(err);
      return Promise.reject(err);
    });
}

exports.tracker = function tracker (req, res) {
  // set JSON content type and CORS headers for the response

  // respond to CORS preflight requests
  if (req.method == 'OPTIONS') {
    setJSONheaders(res);
    res.status(204).send('');
  } else if (req.method == 'HEAD') {
    res.status(200).send('');
  } else if (req.method == 'GET') {
    res.header('Content-Type','text/html');
    res.send(response(req));
  } else if (req.method == 'POST') {
    setJSONheaders(res);
    postPubSub(req.body.name, res);
  } else {
    res.status(501).send('method not supported');
  }
};

// For local testing on node.js (in Docker)
// postPubSub("trick");
